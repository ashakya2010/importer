# Promotools excel to csv (for magento import) converter #

This script convert promotools excel to csv.

### What is this repository for? ###

* Converts excel to csv
* Version 1.0.0
* Usages : https://bitbucket.org/ashakya2010/importer/wiki/Home

### How do I get set up? ###

* Clone url (HTTPS) : git clone https://ashakya2010@bitbucket.org/ashakya2010/importer.git

### Requirements ###
* PHP version: 5.5+

## PHP Excel ## 
* PHP excel library support upto 7.0.6