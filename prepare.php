<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);

define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

date_default_timezone_set('Europe/London');

ini_set('memory_limit', '160M');
ini_set('max_execution_time', 300);

/** Include PHPExcel_IOFactory */
require_once dirname(__FILE__) . '/lib/PHPExcel/Classes/PHPExcel/IOFactory.php';

$inputFileName = "import.xlsx";

if (!file_exists($inputFileName)) {
    exit("File not found." . EOL);
}

try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
        . '": ' . $e->getMessage());
}

//  Get worksheet dimensions
$objPHPExcel->setActiveSheetIndex(0);

$activeSheet = $objPHPExcel->getActiveSheet();


$i = 0;
$row = 3;
$fileCounter = 1;
if(!file_exists('csv'))
{
    mkdir('csv',777);
}

$files = glob('csv/*.csv'); // get all file names
foreach($files as $file){ // iterate files
    if(is_file($file))
        unlink($file); // delete file
}


$fileName = "csv/file".$fileCounter.'.csv';

$fp = fopen($fileName, 'w');

function getAssociatedSkus($sku, $activeSheet, &$row,$fp)
{
    $associatedSkus = [];
    while (true) {
        $row++;

        if (strpos($activeSheet->getCellByColumnAndRow(0, $row)->getValue(), $sku) !== false) {
            $associatedSkus[] = $activeSheet->getCellByColumnAndRow(0, $row)->getValue();
            writeRowInCsv($activeSheet,$row,$fp);
        } else {
            $row--;
            break;
        }
    }

    return $associatedSkus;
}

/*
 *
 * sku	_store	_attribute_set	_type	_category	_root_category	_product_websites	coating
 * color	cost	country_of_manufacture	created_at	custom_design	custom_design_from
 * custom_design_to	custom_layout_update	cutting_speeds	dc	description	diam	diameter	dn
 * ds	gallery	gift_message_available	group	has_options	helix	hex_color_code	image
 * image_label	lc	ln	lt	manufacturer
 * marketing_addition	mat	media_gallery	meta_description	meta_keyword	meta_title	minimal_price	model	msrp	msrp_display_actual_price_type	msrp_enabled	multiselecttest
 * name	news_from_date	news_to_date	options_container	page_layout	power_supplies	price	product_group	radius	required_options	sct	selection_1	selection_2	selection_3	selection_4	selection_5	short_description	small_image	small_image_label	special_from_date	special_price	special_to_date	specific_selection_1	specific_selection_2	specific_selection_3	specific_selection_4	specific_selection_5	specific_selection_6	specific_selection_7	specific_selection_8	status	std	tax_class_id	technical_information	thumbnail	thumbnail_label	type_model	updated_at	url_key	url_path	visibility	weight	z_index	qty	min_qty	use_config_min_qty	is_qty_decimal	backorders	use_config_backorders	min_sale_qty	use_config_min_sale_qty	max_sale_qty	use_config_max_sale_qty	is_in_stock	notify_stock_qty	use_config_notify_stock_qty	manage_stock	use_config_manage_stock	stock_status_changed_auto	use_config_qty_increments	qty_increments	use_config_enable_qty_inc	enable_qty_increments	is_decimal_divided	_links_related_sku	_links_related_position	_links_crosssell_sku	_links_crosssell_position	_links_upsell_sku	_links_upsell_position	_associated_sku	_associated_default_qty	_associated_position	_tier_price_website	_tier_price_customer_group	_tier_price_qty	_tier_price_price	_group_price_website	_group_price_customer_group	_group_price_price	_media_attribute_id	_media_image	_media_lable	_media_position	_media_is_disabled */

$headers = [
    'sku',
    '_store',
    '_attribute_set',
    '_type',
    '_category',
    '_root_category',
    '_product_websites',
    'coating',
    'color',
    'cost',
    'country_of_manufacture',
    'created_at',
    'custom_design',
    'custom_design_from',
    'custom_design_to',
    'custom_layout_update',
    'cutting_speeds',
    'dc',
    'description',
    'diam',
    'diameter',
    'dn',
    'ds',
    'gallery',
    'gift_message_available',
    'group',
    'has_options',
    'helix',
    'hex_color_code',
    'image',
    'image_label',
    'lc',
    'ln',
    'lt',
    'manufacture',
    'marketing_addition',
    'mat',
    'media_gallery',
    'meta_description',
    'meta_keyword',
    'meta_title',
    'minimal_price',
    'model',
    'msrp',
    'msrp_display_actual_price_type',
    'msrp_enabled',
    'multiselecttest',
    'name',
    'news_from_date',
    'news_to_date',
    'options_container',
    'page_layout',
    'power_supplies',
    'price',
    'product_group',
    'radius',
    'required_options',
    'sct',
    'selection_1',
    'selection_2',
    'selection_3',
    'selection_4',
    'selection_5',
    'short_description',
    'small_image',
    'small_image_label',
    'special_from_date',
    'special_price',
    'special_to_date',
    'specific_selection_1',
    'specific_selection_2',
    'specific_selection_3',
    'specific_selection_4',
    'specific_selection_5',
    'specific_selection_6',
    'specific_selection_7',
    'specific_selection_8',
    'status',
    'std',
    'tax_class_id',
    'technical_information',
    'thumbnail',
    'thumbnail_label',
    'type_model',
    'updated_at',
    'url_key',
    'url_path',
    'visibility',
    'weight',
    'z_index',
    'qty',
    'min_qty',
    'use_config_min_qty',
    'is_qty_decimal',
    'backorders',
    'use_config_backorders',
    'min_sale_qty',
    'use_config_min_sale_qty',
    'max_sale_qty',
    'use_config_max_sale_qty',
    'is_in_stock',
    'notify_stock_qty',
    'use_config_notify_stock_qty',
    'manage_stock',
    'use_config_manage_stock',
    'stock_status_changed_auto',
    'use_config_qty_increments',
    'qty_increments',
    'use_config_enable_qty_inc',
    'enable_qty_increments',
    'is_decimal_divided',
    '_links_related_sku',
    '_links_related_position',
    '_links_crosssell_sku',
    '_links_crosssell_position',
    '_links_upsell_sku',
    '_links_upsell_position',
    '_associated_sku',
    '_associated_default_qty',
    '_associated_position',
    '_tier_price_website',
    '_tier_price_customer_group',
    '_tier_price_qty',
    '_tier_price_price',
    '_group_price_website',
    '_group_price_customer_group',
    '_group_price_price',
    '_media_attribute_id',
    '_media_image',
    '_media_lable',
    '_media_position',
    '_media_is_disabled'
];

fputcsv($fp, $headers);
//$csvData[0] = $activeSheet->getCellByColumnAndRow(0,$row)->getValue();
//$csvData[1] = null;
//$csvData[2] = 'Promotools';
//$csvData[3] = ($activeSheet->getCellByColumnAndRow(1,$row)->getValue() == '=')?'simple':'grouped';
//$csvData[4] = ($activeSheet->getCellByColumnAndRow(3,$row)->getValue() == 5)?'Frezen':'';
//$csvData[5] = 'Default Category';
//$csvData[6] = 'base';
//$csvData[7] = $activeSheet->getCellByColumnAndRow(10,$row)->getValue();
//$csvData[8] = null;
//$csvData[9] = null;
//$csvData[10] = null;
//$csvData[11] = null;
//$csvData[12] = null;
//$csvData[13] = null;
//$csvData[14] = null;
//$csvData[15] = null;
//$csvData[16] = $activeSheet->getCellByColumnAndRow(23,$row)->getValue();
//$csvData[17] = $activeSheet->getCellByColumnAndRow(11,$row)->getValue();
//$csvData[18] = $activeSheet->getCellByColumnAndRow(2,$row)->getValue();
//$csvData[19] = $activeSheet->getCellByColumnAndRow(56,$row)->getValue();
//$csvData[20] = $activeSheet->getCellByColumnAndRow(93,$row)->getValue();
//$csvData[21] = $activeSheet->getCellByColumnAndRow(15,$row)->getValue();
//$csvData[22] = $activeSheet->getCellByColumnAndRow(12,$row)->getValue();

$highestActiveSheetRows = $activeSheet->getHighestDataRow();

$counter = 0;
for ($row = 3; $row <= $highestActiveSheetRows; $row++) {

    $groupRow = $row;
    $associatedSkus = [];
    if ($activeSheet->getCellByColumnAndRow(1, $row)->getValue() == "=") {
        $associatedSkus = getAssociatedSkus($activeSheet->getCellByColumnAndRow(0, $row)->getValue(), $activeSheet,$row,$fp);
    }

    writeRowInCsv($activeSheet,$groupRow,$fp,$associatedSkus);

    if($counter >=10)
    {
        fclose($fp);
        $counter = 0;
        $fileCounter ++;

        $fileName = "csv/file".$fileCounter.'.csv';

        $fp = fopen($fileName, 'w');
        fputcsv($fp,$headers);
    }

    $counter++;


}
fclose($fp);

function getCategory($categoryId)
{

    $categories = array(
        19 => 'Klantenservice',
        5 => 'Frezen',
        3 => 'Boren',
        7 => 'Tappen',
        22 => 'Wisselplaten',
        23 => 'Ontbramen',
        24 => 'Spannen',
    );

    return isset($categories[$categoryId])?$categories[$categoryId]:'';

}

function writeRowInCsv($activeSheet,$row,$fp,$associatedSkus=[])
{
    $images = explode(';', $activeSheet->getCellByColumnAndRow(94, $row)->getValue());
    $csvData = [];

    $stackData = [];

    //sku
    $csvData[] = $activeSheet->getCellByColumnAndRow(0, $row)->getValue();
    //_store
    $csvData[] = null;
    //_attribute_set
    $csvData[] = 'Promotools';
    //_type
    $csvData[] = ($activeSheet->getCellByColumnAndRow(1, $row)->getValue() == '=') ? 'grouped' : 'simple';
    //_category
    $csvData[] = getCategory((int) $activeSheet->getCellByColumnAndRow(3, $row)->getValue());
    //_root_category
    $csvData[] = 'Default Category';
    //_product_webistes
    $csvData[] = 'base';
    //coating
    $csvData[] = $activeSheet->getCellByColumnAndRow(10, $row)->getValue();
    //color
    $csvData[] = null;
    //cost
    $csvData[] = null;
    //country_of_manufacture
    $csvData[] = null;
    //created_at
    $csvData[] = null;
    //custom_desing
    $csvData[] = null;
    //custom_design_from
    $csvData[] = null;
    //custom_design_to
    $csvData[] = null;
    //custom_layout_update
    $csvData[] = null;
    //cutting_speeds
    $csvData[] = $activeSheet->getCellByColumnAndRow(23, $row)->getValue();
    //dc
    $csvData[] = $activeSheet->getCellByColumnAndRow(11, $row)->getValue();
    //description
    $csvData[] = $activeSheet->getCellByColumnAndRow(2, $row)->getValue();
    //diam
    $csvData[] = $activeSheet->getCellByColumnAndRow(56, $row)->getValue();
    //diameter
    $csvData[] = $activeSheet->getCellByColumnAndRow(93, $row)->getValue();
    //dn
    $csvData[] = $activeSheet->getCellByColumnAndRow(15, $row)->getValue();
    //ds
    $csvData[] = $activeSheet->getCellByColumnAndRow(12, $row)->getValue();
    //gallery
    $csvData[] = null;
    //gift_message_available
    $csvData[] = null;
    //group
    $csvData[] = $activeSheet->getCellByColumnAndRow(5, $row)->getValue();
    //has_options
    $csvData[] = 0;
    //helix
    $csvData[] = $activeSheet->getCellByColumnAndRow(7, $row)->getValue();
    //hex_color_code
    $csvData[] = $activeSheet->getCellByColumnAndRow(19, $row)->getValue();
    //image
    $csvData[] = $images[0];
    //image_label
    $csvData[] = null;
    //lc
    $csvData[] = $activeSheet->getCellByColumnAndRow(13, $row)->getValue();
    //ln
    $csvData[] = $activeSheet->getCellByColumnAndRow(14, $row)->getValue();
    //lt
    $csvData[] = $activeSheet->getCellByColumnAndRow(16, $row)->getValue();
    //manufacture
    $csvData[] = null;
    //marketing_addition
    $csvData[] = $activeSheet->getCellByColumnAndRow(20, $row)->getValue();
    //mat
    $csvData[] = $activeSheet->getCellByColumnAndRow(5, $row)->getValue();
    //media_gallery
    $csvData[] = null;
    //meta_description
    $csvData[] = null;
    //meta_keyword
    $csvData[] = null;
    //meta_title
    $csvData[] = null;
    //minimal_price
    $csvData[] = null;
    //model
    $csvData[] = $activeSheet->getCellByColumnAndRow(9, $row)->getValue();
    //msrp
    $csvData[] = null;
    //msrp_display_actual_price_type
    $csvData[] = 'Use config';
    //msrp_enabled
    $csvData[] = null;
    //multiselecttest
    $csvData[] = null;
    //name
    $csvData[] = $activeSheet->getCellByColumnAndRow(2, $row)->getValue();
    //news_from_date
    $csvData[] = null;
    //news_to_date
    $csvData[] = null;
    //options_container
    $csvData[] = 'Product Info Column';
    //page_layout
    $csvData[] = null;
    //power_supplies
    $csvData[] = $activeSheet->getCellByColumnAndRow(22, $row)->getValue();
    //price
    $csvData[] = $activeSheet->getCellByColumnAndRow(88, $row)->getValue();
    //product_group
    $csvData[] = $activeSheet->getCellByColumnAndRow(92, $row)->getValue();
    //radius
    $csvData[] = $activeSheet->getCellByColumnAndRow(17, $row)->getValue();
    //require option
    $csvData[] = 0;
    //sct
    $csvData[] = $activeSheet->getCellByColumnAndRow(1,
        $row)->getValue() == '=' ? '' : $activeSheet->getCellByColumnAndRow(1, $row)->getValue();

    //selection 1
    $selection1 = array_filter([
        $activeSheet->getCellByColumnAndRow(24, $row)->getValue() == 1 ? 'UNI' : null,
        $activeSheet->getCellByColumnAndRow(25, $row)->getValue() == 1 ? 'P1' : null,
        $activeSheet->getCellByColumnAndRow(26, $row)->getValue() == 1 ? 'P2' : null,
        $activeSheet->getCellByColumnAndRow(27, $row)->getValue() == 1 ? 'P3' : null,
        $activeSheet->getCellByColumnAndRow(28, $row)->getValue() == 1 ? 'M1' : null,
        $activeSheet->getCellByColumnAndRow(29, $row)->getValue() == 1 ? 'M2' : null,
        $activeSheet->getCellByColumnAndRow(30, $row)->getValue() == 1 ? 'K1' : null,
        $activeSheet->getCellByColumnAndRow(31, $row)->getValue() == 1 ? 'K2' : null,
        $activeSheet->getCellByColumnAndRow(32, $row)->getValue() == 1 ? 'N1' : null,
        $activeSheet->getCellByColumnAndRow(33, $row)->getValue() == 1 ? 'N2' : null,
        $activeSheet->getCellByColumnAndRow(34, $row)->getValue() == 1 ? 'S1' : null,
        $activeSheet->getCellByColumnAndRow(35, $row)->getValue() == 1 ? 'S2' : null,
        $activeSheet->getCellByColumnAndRow(36, $row)->getValue() == 1 ? 'H1' : null,
        $activeSheet->getCellByColumnAndRow(37, $row)->getValue() == 1 ? 'H2' : null,
        $activeSheet->getCellByColumnAndRow(38, $row)->getValue() == 1 ? 'G' : null,
    ]);

    $csvData[] = array_pop($selection1);

    //push to stack
    end($csvData);

    $stackData[key($csvData)] = $selection1;

    //Boren 	Frezen	Tappen	Anders
    //selection_2

    $selection2 = array_filter([
        $activeSheet->getCellByColumnAndRow(39, $row)->getValue() == 1 ? 'Boren' : null,
        $activeSheet->getCellByColumnAndRow(40, $row)->getValue() == 1 ? 'Frezen' : null,
        $activeSheet->getCellByColumnAndRow(41, $row)->getValue() == 1 ? 'Tappen' : null,
        $activeSheet->getCellByColumnAndRow(42, $row)->getValue() == 1 ? 'Anders' : null,
    ]);
    $csvData[] = array_pop($selection2);

    //push to stack
    end($csvData);

    $stackData[key($csvData)] = $selection2;

    //Universal use	HSM	Q/high volume 	Highfeed
    //selection_3

    $selection3 = array_filter([
        $activeSheet->getCellByColumnAndRow(43, $row)->getValue() == 1 ? 'Universal use' : null,
        $activeSheet->getCellByColumnAndRow(44, $row)->getValue() == 1 ? 'HSM' : null,
        $activeSheet->getCellByColumnAndRow(45, $row)->getValue() == 1 ? 'Q/high volume' : null,
        $activeSheet->getCellByColumnAndRow(46, $row)->getValue() == 1 ? 'Highfeed' : null,
    ]);

    $csvData[] = array_pop($selection3);

    end($csvData);

    $stackData[key($csvData)] = $selection3;

    //Spiebaan	hoek	Omtrek	3D
    //selection_4

    $selection4 = array_filter([
        $activeSheet->getCellByColumnAndRow(47, $row)->getValue() == 1 ? 'Spiebaan' : null,
        $activeSheet->getCellByColumnAndRow(48, $row)->getValue() == 1 ? 'hoek' : null,
        $activeSheet->getCellByColumnAndRow(49, $row)->getValue() == 1 ? 'Omtrek' : null,
        $activeSheet->getCellByColumnAndRow(50, $row)->getValue() == 1 ? '3D' : null,
    ]);

    $csvData[] = array_pop($selection4);
    end($csvData);

    $stackData[key($csvData)] = $selection4;

    //Recht	Rad/Torisch	fase	kogel	Afbraam
    //selection_5

    $selection5 = array_filter([
        $activeSheet->getCellByColumnAndRow(51, $row)->getValue() == 1 ? 'Recht' : null,
        $activeSheet->getCellByColumnAndRow(52, $row)->getValue() == 1 ? 'Rad/Torisch' : null,
        $activeSheet->getCellByColumnAndRow(53, $row)->getValue() == 1 ? 'fase' : null,
        $activeSheet->getCellByColumnAndRow(54, $row)->getValue() == 1 ? 'kogel' : null,
        $activeSheet->getCellByColumnAndRow(54, $row)->getValue() == 1 ? 'Afbraam' : null,
    ]);

    $csvData[] = array_pop($selection5);
    end($csvData);

    $stackData[key($csvData)] = $selection5;


    //short_description
    $csvData[] = $activeSheet->getCellByColumnAndRow(2, $row)->getValue();

    //small_image
    $csvData[] = $images[1];
    //small_image_label
    $csvData[] = null;
    //special_from_date
    $csvData[] = null;
    //special_price
    $csvData[] = null;
    //special_to_date
    $csvData[] = null;

    //s	l	xl	xxl	reach
    //specific_selection_1

    $specificSelection1 = array_filter([
        $activeSheet->getCellByColumnAndRow(57, $row)->getValue() == 1 ? 's' : null,
        $activeSheet->getCellByColumnAndRow(58, $row)->getValue() == 1 ? 'l' : null,
        $activeSheet->getCellByColumnAndRow(59, $row)->getValue() == 1 ? 'xl' : null,
        $activeSheet->getCellByColumnAndRow(60, $row)->getValue() == 1 ? 'xxl' : null,
        $activeSheet->getCellByColumnAndRow(61, $row)->getValue() == 1 ? 'reach' : null,
    ]);
    $csvData[] = array_pop($specificSelection1);
    end($csvData);

    $stackData[key($csvData)] = $specificSelection1;

//SC/VHM	PM	HSS	HSSE	HSSV3
    //specific_selection_2
    $specificSelection2 = array_filter([
        $activeSheet->getCellByColumnAndRow(62, $row)->getValue() == 1 ? 'SC/VHM' : null,
        $activeSheet->getCellByColumnAndRow(63, $row)->getValue() == 1 ? 'PM' : null,
        $activeSheet->getCellByColumnAndRow(64, $row)->getValue() == 1 ? 'HSS' : null,
        $activeSheet->getCellByColumnAndRow(65, $row)->getValue() == 1 ? 'HSSE' : null,
        $activeSheet->getCellByColumnAndRow(66, $row)->getValue() == 1 ? 'HSSV3' : null,
    ]);
    $csvData[] = array_pop($specificSelection2);
    end($csvData);

    $stackData[key($csvData)] = $specificSelection2;

//Gecoat	Ongecoat
    //specific_selection_3
    $specificSelection3 = array_filter([
        $activeSheet->getCellByColumnAndRow(67, $row)->getValue() == 1 ? 'Gecoat' : null,
        $activeSheet->getCellByColumnAndRow(68, $row)->getValue() == 1 ? 'Ongecoat' : null,
    ]);
    $csvData[] = array_pop($specificSelection3);
    end($csvData);

    $stackData[key($csvData)] = $specificSelection3;

//z=1	z=2	z=3	z=4	multi
    //specific_selection_4
    $specificSelection4 = array_filter([
        $activeSheet->getCellByColumnAndRow(69, $row)->getValue() == 1 ? 'z=1' : null,
        $activeSheet->getCellByColumnAndRow(70, $row)->getValue() == 1 ? 'z=2' : null,
        $activeSheet->getCellByColumnAndRow(71, $row)->getValue() == 1 ? 'z=3' : null,
        $activeSheet->getCellByColumnAndRow(72, $row)->getValue() == 1 ? 'z=4' : null,
        $activeSheet->getCellByColumnAndRow(73, $row)->getValue() == 1 ? 'multi' : null,
    ]);
    $csvData[] = array_pop($specificSelection4);
    end($csvData);

    $stackData[key($csvData)] = $specificSelection4;

//Trochiodaal	Helicodaal	Ramping	Drilling
    //specific_selection_5
    $specificSelection5 = array_filter([
        $activeSheet->getCellByColumnAndRow(74, $row)->getValue() == 1 ? 'Trochiodaal' : null,
        $activeSheet->getCellByColumnAndRow(75, $row)->getValue() == 1 ? 'Helicodaal' : null,
        $activeSheet->getCellByColumnAndRow(76, $row)->getValue() == 1 ? 'Ramping' : null,
        $activeSheet->getCellByColumnAndRow(77, $row)->getValue() == 1 ? 'Drilling' : null,
    ]);
    $csvData[] = array_pop($specificSelection5);
    end($csvData);

    $stackData[key($csvData)] = $specificSelection5;

//Standaard	Variabele Helix
    //specific_selection_6
    $specificSelection6 = array_filter([
        $activeSheet->getCellByColumnAndRow(78, $row)->getValue() == 1 ? 'Standaard' : null,
        $activeSheet->getCellByColumnAndRow(79, $row)->getValue() == 1 ? 'Variabele Helix' : null,
    ]);
    $csvData[] = array_pop($specificSelection6);
    end($csvData);

    $stackData[key($csvData)] = $specificSelection6;

//HA	HB	HE	PT
    //specific_selection_7
    $specificSelection7 = array_filter([
        $activeSheet->getCellByColumnAndRow(80, $row)->getValue() == 1 ? 'HA' : null,
        $activeSheet->getCellByColumnAndRow(81, $row)->getValue() == 1 ? 'HB' : null,
        $activeSheet->getCellByColumnAndRow(82, $row)->getValue() == 1 ? 'HE' : null,
    ]);
    $csvData[] = array_pop($specificSelection7);
    end($csvData);

    $stackData[key($csvData)] = $specificSelection7;

//PT	dadt
    //specific_selection_8
    $specificSelection8 = array_filter([
        $activeSheet->getCellByColumnAndRow(83, $row)->getValue() == 1 ? 'PT' : null,
        $activeSheet->getCellByColumnAndRow(84, $row)->getValue() == 1 ? 'dadt' : null,
    ]);
    $csvData[] = array_pop($specificSelection8);
    end($csvData);

    $stackData[key($csvData)] = $specificSelection8;

//status
    $csvData[] = 1;

//std
    $csvData[] = $activeSheet->getCellByColumnAndRow(8, $row)->getValue();

//tax class id
    $csvData[] = 2;

//technical information
    $csvData[] = $activeSheet->getCellByColumnAndRow(21, $row)->getValue();

//thumbnail
    $csvData[] = $images[2];
    //thumbnail_label
    $csvData[] = null;
    //type_model
    $csvData[] = null;
    //updated_at
    $csvData[] = null;

    //url key
    $csvData[] = $activeSheet->getCellByColumnAndRow(96, $row)->getValue();

    //url_path
    $csvData[] = null;

    //visibility
    $csvData[] = 4;

    //weight
    $csvData[] = empty(trim($activeSheet->getCellByColumnAndRow(87, $row)->getValue()))?0:$activeSheet->getCellByColumnAndRow(87, $row)->getValue();

    //z
    $csvData[] = $activeSheet->getCellByColumnAndRow(6, $row)->getValue();

    //qty
    $csvData[] = $activeSheet->getCellByColumnAndRow(95, $row)->getValue();
    //min_qty
    $csvData[] = 0;

    //use_config_min_qty
    $csvData[] = 1;
    //is_qty_decimal
    $csvData[] = 0;
    //backorders
    $csvData[] = 0;
    //use_config_backorders
    $csvData[] = 1;

    //min_sele_qty
    $csvData[] = 0;
    //use_config_min_sale_qty
    $csvData[] = 0;
    //max_sale_qty
    $csvData[] = 1;
    //use_config_max_sale_qty
    $csvData[] = 1;
    //is_in_stock
    $csvData[] = 1;

    $csvData[] = 0;
    $csvData[] = 1;
    $csvData[] = 1;
    $csvData[] = 1;
    $csvData[] = 1;
    $csvData[] = 0;
    $csvData[] = 1;
//stock status changed auto
    $csvData[] = 0;
    $csvData[] = 1;
    $csvData[] = 0;
    $csvData[] = 1;
    $csvData[] = 0;
    $csvData[] = 0;

//links releated sku
    $csvData[] = null;
    $csvData[] = null;
    $csvData[] = null;
    if(!empty($associatedSkus))
    {
        $csvData[] = array_pop($associatedSkus);
        end($csvData);
        $stackData[key($csvData)] = $associatedSkus;
    }
    else
    {
        $csvData[] = null;
    }
    $csvData[] = null;

    $csvData[] = null;
    $csvData[] = null;


    $csvData[] = null;
    $csvData[] = null;
    $csvData[] = null;
    $csvData[] = null;
    $csvData[] = null;
    $csvData[] = null;

//media attribute id
    $mediaAttributeIds = array_fill(0,count($images),88);
    $csvData[] = array_pop($mediaAttributeIds);
    end($csvData);
    $stackData[key($csvData)] = $mediaAttributeIds;

    $mediaImages = $images;

    $csvData[] = array_pop($mediaImages);

    end($csvData);
    $stackData[key($csvData)] = $mediaImages;

    $csvData[] = null;
    $c = 1;
    $mediaPositions = array_fill(0,count($images),$c++);
    $csvData[] = array_pop($mediaPositions);
    end($csvData);
    $stackData[key($csvData)] = $mediaPositions;

    $mediaIsDisabled = array_fill(0,count($images),0);
    $csvData[] = array_pop($mediaIsDisabled);
    end($csvData);
    $stackData[key($csvData)] = $mediaIsDisabled;

    end($csvData);

    $lastKeyOfCsv = key($csvData);

    fputcsv($fp, $csvData);


    /**
     * Stack logic to write attributes in stack in next lines
     */
    do {
        $hasArray = false;
        $extraData = [];
        foreach ($stackData as $key => $_stackData) {

            if (!empty($stackData[$key])) {
                $extraData[$key] = array_pop($stackData[$key]);
                $hasArray = $hasArray || true;
            }

        }
        if (!empty($extraData)) {
            for ($i = 0; $i <= $lastKeyOfCsv; $i++) {
                if (!array_key_exists($i, $extraData)) {
                    $extraData[$i] = null;
                }

            }
            ksort($extraData);
            fputcsv($fp, $extraData);
        }

    } while ($hasArray);
}

